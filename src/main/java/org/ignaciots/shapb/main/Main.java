package org.ignaciots.shapb.main;

import org.ignaciots.shapb.cli.SHAPBCommandLine;

/**
 * SHA Parallel Benchmark main class
 * @author Nacho T.
 * @version 1.0
 */

public class Main {

	public static void main(String[] args) {
		new SHAPBCommandLine(args).run();
	}

}
