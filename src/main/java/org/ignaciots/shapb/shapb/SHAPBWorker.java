package org.ignaciots.shapb.shapb;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * SHA Parallel Benchmark worker
 * @author Nacho T.
 * @version 1.0
 */
class SHAPBWorker extends Thread {
	
	private final Random random = new Random();
	private final MessageDigest digest;
	private final int numberOfHashes;
	private final int numberOfBlocks;
	private List<Long> results;
	
	/**
	 * Creates a new SHA Parallel Benchmark worker
	 * @param numberOfHashes Number of hashes that will be calculated by this worker
	 * @param numberOfBlocks Number of blocks that will be used in the hash calculation
	 * @param digestAlgorithm Algorithm that will be used in the hash calculation
	 * @throws NoSuchAlgorithmException Thrown when the provided algorithm cannot be used
	 */
	
	SHAPBWorker(int numberOfHashes, int numberOfBlocks, String digestAlgorithm) throws NoSuchAlgorithmException {
		super();
		this.numberOfHashes = numberOfHashes;
		this.numberOfBlocks = numberOfBlocks;
		this.digest = MessageDigest.getInstance(digestAlgorithm);
	}
	
	@Override
	public void run() {
		List<Long> results = new ArrayList<Long>();
		
		final byte[] digestBytes = new byte[SHAPBMaster.BLOCK_SIZE];
		for (int i = 0; i < this.numberOfHashes; i++) {
			random.nextBytes(digestBytes); //Random bytes are requested once per hash
			Long startTime = System.nanoTime();
			for (int j = 0; j < this.numberOfBlocks; j++) {
				digest.update(digestBytes);
			}
			digest.digest();
			Long endTime = System.nanoTime();
			
			results.add(endTime - startTime);
			digest.reset();
		}
		this.results = results;
	}

	/**
	 * Gets the worker hash time results
	 * @return A list containing the elapsed time for each calculation, in nanoseconds
	 */
	List<Long> getTimeResults() {
		if (results == null) {
			throw new IllegalStateException("Time results can only be obtained after the thread has been run");
		}
		return this.results;
	}

}
