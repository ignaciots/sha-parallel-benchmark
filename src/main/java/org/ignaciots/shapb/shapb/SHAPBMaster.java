package org.ignaciots.shapb.shapb;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.ignaciots.shapb.digest.DigestAlgorithm;
import org.ignaciots.shapb.digest.impl.SHA256Algorithm;

/**
 * SHA Parallel Benchmark master
 * @author Nacho T.
 * @version 1.0
 */
public class SHAPBMaster {
	
	public static final int BLOCK_SIZE = 4096; //Block size used when calculating the digest hash
	public static final int DEFAULT_NUMBER_OF_HASHES = 500;
	public static final int DEFAULT_NUMBER_OF_BLOCKS = 1024;
	public static final DigestAlgorithm DEFAULT_DIGEST_ALGORITHM = new SHA256Algorithm();
	
	private Long totalTime;
	private int numberOfThreads; //the number of threads that will be used to calculate the workers
	private final int numberOfHashes; //the number of calculated hashes per worker
	private final int numberOfBlocks; //the number of 1 KiB blocks used per hash
	private final DigestAlgorithm digestAlgorithm; //the used digest algorithm
	
	/**
	 * Crates a new SHA Parallel Benchmark master with default parameters
	 */
	public SHAPBMaster() {
		this(DEFAULT_NUMBER_OF_HASHES, DEFAULT_NUMBER_OF_BLOCKS, DEFAULT_DIGEST_ALGORITHM);
	}
	
	/**
	 * Creates a new SHA Parallel Benchmark master with a default number of threads
 	 * @param numberOfHashes Number of hashes that will be calculated by this worker
	 * @param numberOfBlocks Number of blocks that will be used in the hash calculation
	 * @param digestAlgorithm Algorithm that will be used in the hash calculation
	 * @throws NoSuchAlgorithmException Thrown when the provided algorithm cannot be used
	 */
	public SHAPBMaster(int numberOfHashes, int numberOfBlocks, DigestAlgorithm digestAlgorithm) {
		if (numberOfHashes < 1) {
			throw new IllegalArgumentException("The number of calculated hashes must be > 0");
		}
		if (numberOfBlocks < 1) {
			throw new IllegalArgumentException("The number of blocks must be > 0");
		}
		this.numberOfHashes = numberOfHashes;
		this.numberOfBlocks = numberOfBlocks;
		this.digestAlgorithm = digestAlgorithm;
		this.numberOfThreads = Runtime.getRuntime().availableProcessors();
	}
	
	/**
	 * Creates a new SHA Parallel Benchmark master with a default number of threads
 	 * @param numberOfHashes Number of hashes that will be calculated by this worker
	 * @param numberOfBlocks Number of blocks that will be used in the hash calculation
	 * @param digestAlgorithm Algorithm that will be used in the hash calculation
	 * @param numberOfThreads The number of threads that will be used in the calculation
	 * @throws NoSuchAlgorithmException Thrown when the provided algorithm cannot be used
	 */
	public SHAPBMaster(int numberOfHashes, int numberOfBlocks, DigestAlgorithm digestAlgorithm, int numberOfThreads) {
		this(numberOfHashes, numberOfBlocks, digestAlgorithm);
		if (numberOfThreads < 1) {
			throw new IllegalArgumentException("The number of threads must be > 0");
		}
		this.numberOfThreads = numberOfThreads;
	}
	
	/**
	 * Starts the hash calculation for each thread
	 * @return A list containing the time results of each thread, in nanoseconds
	 * @throws InterruptedException 
	 * @throws NoSuchAlgorithmException 
	 */
	public List<List<Long>> calculateHashes() throws InterruptedException, NoSuchAlgorithmException {
		final List<List<Long>> timeResults = new ArrayList<List<Long>>();
		final List<SHAPBWorker> workers = new ArrayList<SHAPBWorker>();
		
		Long starTime = System.nanoTime();
		for (int i = 0; i < this.numberOfThreads; i++) {
			SHAPBWorker worker = new SHAPBWorker(numberOfHashes, numberOfBlocks, digestAlgorithm.getAlgorithmName());
			worker.setDaemon(true);
			workers.add(worker);
			worker.start();
		}
		
		for (SHAPBWorker worker : workers) {
			worker.join();
			timeResults.add(worker.getTimeResults());
		}
		Long endTime = System.nanoTime();
		this.totalTime = endTime - starTime;
		
		return timeResults;
	}
	
	/**
	 * Gets the elapsed tiem after the hashes calculation
	 * @return The elapsed time, in nanoseconds
	 */
	public Long getTotalTime() {
		if (this.totalTime == null) {
			throw new IllegalStateException("The total time can oly be obtained after the hashes calculation");
		}
		return this.totalTime;
	}
	
	/**
	 * Gets the number of threads used in the hashes calculation
	 * @return The number of used threads
	 */
	public int getNumberOfThreads() {
		return this.numberOfThreads;
	}
	
	/**
	 * Gets the number of calculated hashes per thread
	 * @return The number of hashes per thread
	 */
	public int getNumberOfHashes() {
		return this.numberOfHashes;
	}
	
	/**
	 * Get the number of blocks used per hash
	 * @return The number 1 KiB blocks used per hash
	 */
	public int getNumberOfBlocks() {
		return this.numberOfBlocks;
	}
	
	/**
	 * Gets the name of the used digest algorithm
	 * @return The name of the used digest algorithm
	 */
	public DigestAlgorithm getDigestAlgorithm() {
		return this.digestAlgorithm;
	}

}
