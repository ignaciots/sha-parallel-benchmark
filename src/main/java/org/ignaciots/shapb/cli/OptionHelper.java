package org.ignaciots.shapb.cli;

import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

/**
 * Class that declares the options and arguments used by the program
 * @author Nacho T.
 * @version 1.0
 */

public class OptionHelper {
	
	//Options
	public static final String HELP_OPTION_SHORTNAME = "h";
	public static final String HELP_OPTION_LONGNAME= "help";
	public static final String HELP_OPTION_DESCRIPTION = "Shows this program's help";
	public static final String VERSION_OPTION_SHORTNAME = "v";
	public static final String VERSION_OPTION_LONGNAME = "version";
	public static final String VERSION_OPTION_DESCRIPTION= "Shows this program's name and version";
	public static final String HASHES_OPTION_SHORTNAME = "hs";
	public static final String HASHES_OPTION_LONGNAME = "hashes";
	public static final String HASHES_OPTION_DESCRIPTION = "Sets the number of calculated hashes (default is 1000)";
	public static final String BLOCKS_OPTION_SHORTNAME = "b";
	public static final String BLOCKS_OPTION_LONGNAME = "blocks";
	public static final String BLOCKS_OPTION_DESCRIPTION = "Sets the number of blocks to be used in the algorithm (1 block = 4096 KiB; default number is 2500)";
	public static final String THREADS_OPTION_SHORTNAME = "t";
	public static final String THREADS_OPTION_LONGNAME = "threads";
	public static final String THREADS_OPTION_DESCRIPTION = "Sets the number of threads used (default is number of system CPU threads)";
	public static final String MD2_OPTION_SHORTNAME = "m2";
	public static final String MD2_OPTION_LONGNAME = "md2";
	public static final String MD2_OPTION_DESCRIPTION = "Uses MD2 checksum algorithm";
	public static final String MD5_OPTION_SHORTNAME = "m5";
	public static final String MD5_OPTION_LONGNAME = "md5";
	public static final String MD5_OPTION_DESCRIPTION = "Uses MD5 checksum algorithm";
	public static final String SHA1_OPTION_SHORTNAME = "s1";
	public static final String SHA1_OPTION_LONGNAME = "sha1";
	public static final String SHA1_OPTION_DESCRIPTION = "Uses SHA1 checksum algorithm";
	public static final String SHA224_OPTION_SHORTNAME = "s22";
	public static final String SHA224_OPTION_LONGNAME = "sha224";
	public static final String SHA224_OPTION_DESCRIPTION = "Uses SHA224 checksum algorithm";
	public static final String SHA256_OPTION_SHORTNAME = "s25";
	public static final String SHA256_OPTION_LONGNAME = "sha256";
	public static final String SHA256_OPTION_DESCRIPTION = "Uses SHA256 checksum algorithm";
	public static final String SHA384_OPTION_SHORTNAME = "s3";
	public static final String SHA384_OPTION_LONGNAME = "sha384";
	public static final String SHA384_OPTION_DESCRIPTION = "Uses SHA384 checksum algorithm";
	public static final String SHA512_OPTION_SHORTNAME = "s5";
	public static final String SHA512_OPTION_LONGNAME = "sha512";
	public static final String SHA512_OPTION_DESCRIPTION = "Uses SHA512 checksum algorithm";
	
	private static Options options = null;
	
	/**
	 * Gets the options used by the program
	 * @return A Options object containing the options used by the program
	 */
	public static Options getOptions() {
		if (options == null) {
			Options newOptions = new Options();
			OptionGroup mainOptions = new OptionGroup(); //Mutually exclusive options
			mainOptions.addOption(new Option(MD2_OPTION_SHORTNAME, MD2_OPTION_LONGNAME, false, MD2_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(MD5_OPTION_SHORTNAME, MD5_OPTION_LONGNAME, false, MD5_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(SHA1_OPTION_SHORTNAME, SHA1_OPTION_LONGNAME, false, SHA1_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(SHA224_OPTION_SHORTNAME, SHA224_OPTION_LONGNAME, false, SHA224_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(SHA256_OPTION_SHORTNAME, SHA256_OPTION_LONGNAME, false, SHA256_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(SHA384_OPTION_SHORTNAME, SHA384_OPTION_LONGNAME, false, SHA384_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(SHA512_OPTION_SHORTNAME, SHA512_OPTION_LONGNAME, false, SHA512_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(HELP_OPTION_SHORTNAME, HELP_OPTION_LONGNAME, false, HELP_OPTION_DESCRIPTION));
			mainOptions.addOption(new Option(VERSION_OPTION_SHORTNAME, VERSION_OPTION_LONGNAME, false, VERSION_OPTION_DESCRIPTION));
			newOptions.addOptionGroup(mainOptions);
			newOptions.addOption(new Option(HASHES_OPTION_SHORTNAME, HASHES_OPTION_LONGNAME, true, HASHES_OPTION_DESCRIPTION));
			newOptions.addOption(new Option(BLOCKS_OPTION_SHORTNAME, BLOCKS_OPTION_LONGNAME, true, BLOCKS_OPTION_DESCRIPTION));
			newOptions.addOption(new Option(THREADS_OPTION_SHORTNAME, THREADS_OPTION_LONGNAME, true, THREADS_OPTION_DESCRIPTION));
			options = newOptions;
		}

		return options;
	}
	
	/**
	 * Pretty formats an option group
	 * @param optionGroup The option group to be pretty formatted
	 * @return The resulting pretty formatted string
	 */
	public static String prettyOptionGroup(OptionGroup optionGroup) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Option option : optionGroup.getOptions()) {
			builder.append("-" + option.getOpt());
			if (!(optionGroup.getOptions().size() - 1 == i)) {
				builder.append(", ");
			}
			i++;
		}
		builder.append("]");
		
		return builder.toString();

	}
	
	/**
	 * Pretty formats a missing options list
	 * @param missingOptions The missing options list to be pretty formatted
	 * @return The resulting pretty formatted string
	 */
	public static String prettyMissingOptions(List<?> missingOptions) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Object option : missingOptions) { //List contains OptionGroup and Option
			if (option instanceof OptionGroup) { //Option group
				builder.append(prettyOptionGroup((OptionGroup) option));
			} else { //Option
				builder.append("-" + option.toString());
			}
			if (!(missingOptions.size() - 1 == i)) {
				builder.append(", ");
			}
			i++;
		}
		builder.append("]");
		
		return builder.toString();
	}
	
	/**
	 * Pretty prints an option
	 * @param option The option to be pretty printed
	 * @return The resulting pretty printed string
	 */
	public static String prettyOption(Option option) {
		StringBuilder builder = new StringBuilder();
		builder.append("-");
		builder.append(option.getOpt());
		if (option.getLongOpt() != null) {
			builder.append(" --");
			builder.append(option.getLongOpt());
		}
		builder.append(" :: ");
		builder.append(option.getDescription());
		
		return builder.toString();
	}
}
