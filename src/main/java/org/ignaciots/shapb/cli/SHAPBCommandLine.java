package org.ignaciots.shapb.cli;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.commons.cli.AlreadySelectedException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.ignaciots.shapb.digest.DigestAlgorithm;
import org.ignaciots.shapb.digest.impl.*;
import org.ignaciots.shapb.shapb.SHAPBMaster;

/**
 * SHAPB command line program
 * @author Nacho T.
 * @version 1.0
 */

public class SHAPBCommandLine {
	
	public static final String PROGRAM_NAME = "shapb";
	public static final String PROGRAM_LONG_NAME = "SHA Parallel Benchmark";
	public static final String PROGRAM_AUTHOR = "Nacho T.";
	public static final String PROGRAM_VERSION = "1.0.0";
	public static final String PROGRAM_HEADER = "CPU benchmarking program with different digest algorithms";
	public static final int INVALID_ARG_EXIT_CODE = 3; // Exit code for invalid options
	public static final int FORMAT_ERROR_EXIT_CODE = 4; //Exit code for invalid argument format
	public static final int FATAL_ERROR_EXIT_CODE = 10; //Exit code for fatal errors
	
	private final String[] args;
	private final Options options = OptionHelper.getOptions();
	
	/**
	 * Creates a SHAPB CLI program
	 * @param args The program arguments
	 */
	public SHAPBCommandLine(String[] args) {
		this.args = args;
	}
	
	/**
	 * Runs the SHAPB CLI program
	 */
	public void run() {
		CommandLine commandLine = null;
		
		try {
			commandLine = new DefaultParser().parse(this.options, this.args);
		} catch (AlreadySelectedException e) {
			handleAlreadySelectedException(e);
		} catch (MissingArgumentException e) {
			handleMissingArgumentException(e);
		} catch (MissingOptionException e) {
			handleMissingOptionException(e);
		} catch (UnrecognizedOptionException e) {
			handleUnrecognizedOptionException(e);
		} catch (ParseException e) {
			handleParseException(e);
		}
		
		if (commandLine.hasOption(OptionHelper.HELP_OPTION_SHORTNAME)) {
			showHelp();
		}
		if (commandLine.hasOption(OptionHelper.VERSION_OPTION_SHORTNAME)) {
			showVersion();
		}

		int hashes = SHAPBMaster.DEFAULT_NUMBER_OF_HASHES;
		int blocks = SHAPBMaster.DEFAULT_NUMBER_OF_BLOCKS;
		Integer threads = null;
		DigestAlgorithm algorithm = SHAPBMaster.DEFAULT_DIGEST_ALGORITHM;
		if (commandLine.hasOption(OptionHelper.HASHES_OPTION_SHORTNAME)) {
			String value = commandLine.getOptionValue(OptionHelper.HASHES_OPTION_SHORTNAME);
			try {
				hashes = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				handleNumberFormatException(
						String.format("Invalid value for hashes: %s : Value must be an integer", value));
			}
		}
		if (commandLine.hasOption(OptionHelper.BLOCKS_OPTION_SHORTNAME)) {
			String value = commandLine.getOptionValue(OptionHelper.BLOCKS_OPTION_SHORTNAME);
			try {
				blocks = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				handleNumberFormatException(
						String.format("Invalid value for blocks: %s : Value must be an integer", value));
			}
		}
		if (commandLine.hasOption(OptionHelper.THREADS_OPTION_SHORTNAME)) {
			String value = commandLine.getOptionValue(OptionHelper.THREADS_OPTION_SHORTNAME);
			try {
				threads = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				handleNumberFormatException(
						String.format("Invalid value for threads: %s : Value must be an integer", value));
			}
		}
	
		
		SHAPBMaster master = null;
		if (commandLine.hasOption(OptionHelper.MD2_OPTION_SHORTNAME)) {
			algorithm = new MD2Algorithm();
		} else if (commandLine.hasOption(OptionHelper.MD5_OPTION_SHORTNAME)) {
			algorithm = new MD5Algorithm();
		} else if (commandLine.hasOption(OptionHelper.SHA1_OPTION_SHORTNAME)) {
			algorithm = new SHA1Algorithm();
		} else if (commandLine.hasOption(OptionHelper.SHA224_OPTION_SHORTNAME)) {
			algorithm = new SHA224Algorithm();
		} else if (commandLine.hasOption(OptionHelper.SHA256_OPTION_SHORTNAME)) {
			algorithm = new SHA256Algorithm();
		} else if (commandLine.hasOption(OptionHelper.SHA384_OPTION_SHORTNAME)) {
			algorithm = new SHA384Algorithm();
		} else if (commandLine.hasOption(OptionHelper.SHA512_OPTION_SHORTNAME)) {
			algorithm = new SHA512Algorithm();
		}
		
		if (threads == null) {
			master = new SHAPBMaster(hashes, blocks, algorithm);
		} else {
			master = new SHAPBMaster(hashes, blocks, algorithm, threads);
		}
		
		try {
			showResults(master);
		} catch (NoSuchAlgorithmException e) {
			handleNoSuchAlgorithmException(e);
		} catch (InterruptedException e) {
			handleInterruptedException(e);
		}
		
	}
	
	private void showHelp() {
		new HelpFormatter().printHelp(PROGRAM_NAME, PROGRAM_HEADER, this.options, null, true);
		System.exit(0);
	}
	
	private void showVersion() {
		System.out.println(String.format("%s version %s\nCopyright \u00A9 %s", PROGRAM_LONG_NAME, PROGRAM_VERSION,
				PROGRAM_AUTHOR));
		System.exit(0);
	}
	
	private void showResults(SHAPBMaster master) throws NoSuchAlgorithmException, InterruptedException {
		System.out.println("SHAPB started...");
		List<List<Long>> results = master.calculateHashes();
		System.out.println("SHAPB ended. Showing results...");
		Long totalTime = master.getTotalTime();
		printStats(master);
		printResults(results, master);
		printTotalTime(totalTime);
	}
	
	private void printStats(SHAPBMaster master) {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("Used algorithm: %s\n", master.getDigestAlgorithm().getAlgorithmName()));
		builder.append(String.format("Number of used threads: %d\n", master.getNumberOfThreads()));
		builder.append(String.format("Number of hashes per thread: %d\n", master.getNumberOfHashes()));
		builder.append(String.format("Number of used blocks per hash: %d (%d KiB)\n", master.getNumberOfBlocks(),
				master.getNumberOfBlocks() * SHAPBMaster.BLOCK_SIZE));
		System.out.println(builder.toString());
	}
	
	private void printResults(List<List<Long>> results, SHAPBMaster master) {
		StringBuilder builder = new StringBuilder();
		int threadNumber = 1;
		for (List<Long> resultList : results) {
			long resultSum = 0;
			for (Long result : resultList) {
				resultSum += result;
			}
			Long resultAverage = resultSum / resultList.size();
			Long resultAverageSeconds = (resultAverage) / 1000000000; //nano second divide
			//Long kibPerSecondRate = ((master.getNumberOfBlocks() *  SHAPBMaster.BLOCK_SIZE) / resultAverage) / 1000000000;
			builder.append(String.format("Thread %d average hash computation: %s seconds (%s nanoseconds)\n",
					threadNumber, resultAverageSeconds.toString(), resultAverage.toString()));
			//builder.append(String.format("Thread %d KiB per second hash rate: %s KiB/s\n", threadNumber, kibPerSecondRate.toString()));
			threadNumber++; //TODO Fix KIB/s division
		}
		System.out.println(builder.toString());
	}
	
	private void printTotalTime(Long totalTime) {
		Long totalTimeSeconds = totalTime / 1000000000;
		System.out.println(String.format("Total elapsed time: %s seconds (%s nanoseconds)", totalTimeSeconds, totalTime));
	}

	private void handleAlreadySelectedException(AlreadySelectedException e) {
		String cause = e.getOption().getOpt();
		String optionGroup = OptionHelper.prettyOptionGroup(e.getOptionGroup());
		System.err.println(
				String.format("Error: invalid option: %s: More than one of the following options has been chosen: %s\n"
						+ "Use %s -h for more help.", cause, optionGroup, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleMissingArgumentException(MissingArgumentException e) {
		String cause = e.getOption().getOpt();
		System.err.println(String.format("Error: argument for option -%s has not been provided\n" + "Use %s -h for more help.",
				cause, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleMissingOptionException(MissingOptionException e) {
		String cause = OptionHelper.prettyMissingOptions(e.getMissingOptions());
		System.err.println(String.format("Error: one option from each of the following option groups is missing: %s\n" + "Use %s -h for more help.", cause,
				PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleUnrecognizedOptionException(UnrecognizedOptionException e) {
		String cause = e.getOption();
		System.err.println(
				String.format("Error: unrecognized options: %s\n" + "Use %s -h for more help.", cause, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleParseException(ParseException e) {
		e.printStackTrace();
		System.err.println("Error: fatal error parsing arguments.\nExiting...");
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleNumberFormatException(String message) {
		System.err.println(message);
		System.exit(FORMAT_ERROR_EXIT_CODE);
	}
	
	private void handleNoSuchAlgorithmException(NoSuchAlgorithmException e) {
		System.err.println("Error: fatal error, invalid hash algorithm.\n");
		e.printStackTrace();
		System.exit(FATAL_ERROR_EXIT_CODE);
	}
	
	private void handleInterruptedException(InterruptedException e) {
		System.err.println("Error: fatal error, thread could not be interrupted");
		e.printStackTrace();
		System.exit(FATAL_ERROR_EXIT_CODE);
	}

}
