package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest MD5 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class MD5Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "MD5";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
