package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest SHA-224 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA224Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "SHA-224";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
