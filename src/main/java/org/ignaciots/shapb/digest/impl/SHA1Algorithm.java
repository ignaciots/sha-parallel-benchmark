package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest SHA-1 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA1Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "SHA-1";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
