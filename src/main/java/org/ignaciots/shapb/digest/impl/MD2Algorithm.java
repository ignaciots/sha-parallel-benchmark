package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest MD2 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class MD2Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "MD2";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
