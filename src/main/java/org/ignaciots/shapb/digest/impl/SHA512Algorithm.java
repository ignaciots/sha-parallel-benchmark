package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest SHA-512 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA512Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "SHA-512";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
