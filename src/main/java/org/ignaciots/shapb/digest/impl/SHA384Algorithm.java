package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest SHA-384 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA384Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "SHA-384";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
