package org.ignaciots.shapb.digest.impl;

import org.ignaciots.shapb.digest.DigestAlgorithm;

/**
 * MessageDigest SHA-256 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA256Algorithm implements DigestAlgorithm {
	
	private final String algorithmName = "SHA-256";

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
