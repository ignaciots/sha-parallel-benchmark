package org.ignaciots.shapb.digest;

/**
 * Class that represents one of the MessageDigest algorithms
 * @author Nacho T.
 * @version 1.0
 */
public interface DigestAlgorithm {
	/**
	 * Gets the name of the associated algorithm
	 * 
	 * @return The name of the associated algorithm, usable to obtain a
	 *         MessageDigest instance
	 */
	String getAlgorithmName();
}
