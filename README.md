# sha-parallel-benchmark

Aplicación de benchmark usando funciones hash en varios núcleos.

## Uso

Ver ayuda:

```
usage: shapb [-b <arg>] [-h | -m2 | -m5 | -s1 | -s22 | -s25 | -s3 | -s5 |
       -v] [-hs <arg>]        [-t <arg>]
CPU benchmarking program with different digest algorithms
 -b,--blocks <arg>    Sets the number of blocks to be used in the
                      algorithm (1 block = 4096 KiB; default number is
                      2500)
 -h,--help            Shows this program's help
 -hs,--hashes <arg>   Sets the number of calculated hashes (default is
                      1000)
 -m2,--md2            Uses MD2 checksum algorithm
 -m5,--md5            Uses MD5 checksum algorithm
 -s1,--sha1           Uses SHA1 checksum algorithm
 -s22,--sha224        Uses SHA224 checksum algorithm
 -s25,--sha256        Uses SHA256 checksum algorithm
 -s3,--sha384         Uses SHA384 checksum algorithm
 -s5,--sha512         Uses SHA512 checksum algorithm
 -t,--threads <arg>   Sets the number of threads used (default is number
                      of system CPU threads)
 -v,--version         Shows this program's name and version
```
